<?php

/**
 * @file
 * Mailchimp administration settings.
 *
 */

/**
 * Mailchimp settings on checkout admin page
 */
function uc_mailchimp_pane_settings_pane() {
  $form = array();

  $lists = mailchimp_get_lists();
  foreach ($lists as $list_id => $value) {
    if ($value) {
      $options[$value->id] = $value->name;
    }
  }

  $form['uc_mailchimp_pane_list'] = array(
    '#type' => 'select',
    '#title' => t('Select Mailchimp List'),
    '#options' => $options,
    '#default_value' =>  variable_get('uc_mailchimp_pane_list', array()),
    '#multiple' => TRUE,
    '#description' => t('You can select multiple maichimp lists.'),
  );
  $form['uc_mailchimp_pane_checkout_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Mailchimp Checkout Title'),
    '#default_value' => variable_get('uc_mailchimp_pane_checkout_title', t('Mailchimp')),
    '#description' => t(''),
  );
  $form['uc_mailchimp_pane_description'] = array(
    '#type' => 'textarea',
    '#title' => t('Description'),
    '#default_value' => variable_get('uc_mailchimp_pane_description', ''),
    '#description' => t('Some description to display under the Mailchimp title on checkout.'),
  );
  $form['uc_mailchimp_pane_default'] = array(
    '#type' => 'checkbox',
    '#title' => t('Selected by default?'),
    '#default_value' => variable_get('uc_mailchimp_pane_default', TRUE),
    '#description' => t('Should the mailchimp list on checkout page be selected by default?'),
  );

  return $form;
}
